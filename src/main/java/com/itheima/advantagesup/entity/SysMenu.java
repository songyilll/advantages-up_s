package com.itheima.advantagesup.entity;

import com.itheima.advantagesup.vo.MenuVO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author zhengkai.blog.csdn.net
 * @description menu
 * @date 2023-07-26
 */
@Data
public class SysMenu implements Serializable,Entity<MenuVO> {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer pid;
    private String path;
    private String component;
    private String title;
    private String icon;
    private Integer isShow;
    private Integer isCache;
    private Integer isLink;
    private String redirect;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private Integer delFlag;
    private Integer type;
    private BigDecimal sort;
    private String mark;
    public MenuVO conversion(){
        MenuVO menuVO = new MenuVO();
        menuVO.setId(id);
        menuVO.setPid(pid);
        menuVO.setPath(path);
        menuVO.setComponent(component);
        menuVO.setTitle(title);
        menuVO.setIcon(icon);
        menuVO.setIs_show(isShow);
        menuVO.setIs_cache(isCache);
        menuVO.setIs_link(isLink);
        menuVO.setRedirect(redirect);
        menuVO.setCreateDate(createDate);
        menuVO.setUpdateDate(updateDate);
        menuVO.setDel_flag(delFlag);
        menuVO.setType(type);
        menuVO.setSort(sort);
        menuVO.setMark(mark);
        return menuVO;
    }
}