package com.itheima.advantagesup.entity;

import lombok.Data;

@Data
public class YddTag {
    private Integer id;
    private String tagname;
}
