package com.itheima.advantagesup.entity;

import com.itheima.advantagesup.mapper.SysUserMapper;
import com.itheima.advantagesup.vo.RoleVO;
import com.itheima.advantagesup.vo.UserInfoVO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author zhengkai.blog.csdn.net
 * @description user
 * @date 2023-07-26
 */
@Data
public class SysUser implements Serializable, Entity<UserInfoVO> {

    private static final long serialVersionUID = 1L;
    private String id;
    private String loginName;
    private String password;
    private String name;
    private String email;
    private String phone;
    private String loginIp;
    private LocalDateTime loginDate;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private String delFlag;
    private String roleId;
    private Integer status;
    private String roleName;

    public SysUser() {

    }

    public UserInfoVO conversion() {
        UserInfoVO userInfoVO = new UserInfoVO();
        userInfoVO.setId(id);
        userInfoVO.setLogin_name(loginName);
        userInfoVO.setPassword(password);
        userInfoVO.setName(name);
        userInfoVO.setEmail(email);
        userInfoVO.setPhone(phone);
        userInfoVO.setLogin_ip(loginIp);
        userInfoVO.setLogin_date(loginDate);
        userInfoVO.setCreate_date(createDate);
        userInfoVO.setUpdate_date(updateDate);
        userInfoVO.setDel_flag(delFlag);

        userInfoVO.setStatus(status);

        List<String> stringList1 = new ArrayList<>(Arrays.asList(roleId.replace("\"", "").replace("[", "").replace("]", "").split(",")));
        userInfoVO.setRole_id(stringList1);
        return userInfoVO;
    }

}