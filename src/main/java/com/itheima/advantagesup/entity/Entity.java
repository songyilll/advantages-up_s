package com.itheima.advantagesup.entity;

import com.itheima.advantagesup.vo.MenuVO;

public interface Entity<Y> {
    Y conversion();
}
