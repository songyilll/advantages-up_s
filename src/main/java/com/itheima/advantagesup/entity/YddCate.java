package com.itheima.advantagesup.entity;

import com.itheima.advantagesup.vo.CateVO;
import lombok.Data;

@Data
public class YddCate implements Entity<CateVO> {
    private Integer id;
    private String catename;
    private String icon;
    private Integer sortNum;
    private String defaultData;

    public CateVO conversion(){

        CateVO cateVO = new CateVO();

        cateVO.setId(id);
        cateVO.setCatename(catename);
        cateVO.setIcon(icon);
        cateVO.setSort_num(sortNum);
        cateVO.setDefault_data(defaultData);

        return cateVO;
    }
}
