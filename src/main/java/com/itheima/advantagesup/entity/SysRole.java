package com.itheima.advantagesup.entity;

import com.itheima.advantagesup.vo.MenuVO;
import com.itheima.advantagesup.vo.RoleVO;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SysRole implements Entity<RoleVO> {
    private String id;
    private String name;
    private String description;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private String remarks;
    private String delFlag;
    private Integer superadmin;
    private String defaultData;
    private String roleId;
    private String roleMenus;

    public RoleVO conversion(){
        RoleVO roleVO = new RoleVO();
        roleVO.setId(id);
        roleVO.setName(name);
        roleVO.setDescription(description);
        roleVO.setCreate_date(createDate);
        roleVO.setUpdate_date(LocalDateTime.now());
        roleVO.setRemarks(remarks);
        roleVO.setDel_flag(delFlag);
        roleVO.setSuperadmin(superadmin);
        roleVO.setDefault_data(defaultData);
        roleVO.setRole_id(roleId);
        roleVO.setRole_menus(roleMenus);
        return roleVO;
    }
}
