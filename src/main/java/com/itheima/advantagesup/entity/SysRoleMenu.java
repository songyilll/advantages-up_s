package com.itheima.advantagesup.entity;

import lombok.Data;

@Data
public class SysRoleMenu {
    private String roleId;
    private Integer menuId;
}
