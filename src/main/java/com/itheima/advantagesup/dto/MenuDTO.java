package com.itheima.advantagesup.dto;

import com.itheima.advantagesup.entity.SysMenu;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class MenuDTO {
    private Integer id;
    private Integer pid;
    private String path;
    private String component;
    private String title;
    private String icon;
    private Integer is_show;
    private Integer is_cache;
    private Integer is_link;
    private String redirect;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private Integer del_flag;
    private Integer type;
    private BigDecimal sort;
    private String mark;

    public SysMenu conversion(){
        SysMenu sysMenu = new SysMenu();
        sysMenu.setId(id);
        sysMenu.setPid(pid);
        sysMenu.setPath(path);
        sysMenu.setComponent(component);
        sysMenu.setTitle(title);
        sysMenu.setIcon(icon);
        sysMenu.setIsShow(is_show);
        sysMenu.setIsCache(is_cache);
        sysMenu.setIsLink(is_link);
        sysMenu.setRedirect(redirect);
        sysMenu.setCreateDate(createDate);
        sysMenu.setUpdateDate(updateDate);
        sysMenu.setDelFlag(del_flag);
        sysMenu.setType(type);
        sysMenu.setSort(sort);
        sysMenu.setMark(mark);

        return sysMenu;
    }
}
