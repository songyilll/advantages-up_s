package com.itheima.advantagesup.dto;

import com.itheima.advantagesup.entity.SysRole;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RoleDTO {
    private String name;
    private LocalDateTime create_date;
    private String default_data;
    private String del_flag;
    private String id;
    private String remarks;
    private Integer superadmin;
    private LocalDateTime update_date;
    private String description;
    private Integer[] menuids;

    public SysRole conversion (){
        SysRole sysRole = new SysRole();
        sysRole.setId(id);
        sysRole.setName(name);
        sysRole.setDescription(description);
        sysRole.setCreateDate(create_date);
        sysRole.setUpdateDate(update_date);
        sysRole.setRemarks(remarks);
        sysRole.setDelFlag(del_flag);
        sysRole.setSuperadmin(superadmin);
        sysRole.setDefaultData(default_data);
        return sysRole;
    }
}
