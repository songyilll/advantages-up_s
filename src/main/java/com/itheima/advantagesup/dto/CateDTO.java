package com.itheima.advantagesup.dto;

import com.itheima.advantagesup.entity.YddCate;
import lombok.Data;

@Data
public class CateDTO {
    private Integer id;
    private String catename;
    private Integer sort_num;
    private String icon;

    public YddCate conversion(){
        YddCate yddCate = new YddCate();
        yddCate.setId(id);
        yddCate.setCatename(catename);
        yddCate.setIcon(icon);
        yddCate.setSortNum(sort_num);

        return yddCate;
    }
}
