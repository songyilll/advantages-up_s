package com.itheima.advantagesup.dto;

import com.itheima.advantagesup.entity.SysUser;
import lombok.Data;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.StringJoiner;

@Data
public class UserDTO {
    private String id;
    private String name;
    private String login_name;
    private String password;
    private String email;
    private String phone;
    private List<String> role_id;
    private Integer status;

    public SysUser conversion(){
        SysUser sysUser = new SysUser();
        sysUser.setId(id);
        sysUser.setLoginName(login_name);
        sysUser.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
        sysUser.setName(name);
        sysUser.setEmail(email);
        sysUser.setPhone(phone);
        sysUser.setCreateDate(LocalDateTime.now());
        sysUser.setUpdateDate(LocalDateTime.now());

        StringJoiner stringJoiner = new StringJoiner("\",\"","[\"","\"]");
        role_id.forEach(stringJoiner::add);
        sysUser.setRoleId(stringJoiner.toString());

        sysUser.setStatus(status);
       return sysUser;
    }
}
