package com.itheima.advantagesup.mapper;


import com.itheima.advantagesup.entity.YddCate;
import lombok.Data;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface YddCateMapper {

    @Select("select * from advantages_up.ydd_cate")
    List<YddCate> selectAll();

    @Select("select * from advantages_up.ydd_cate where catename like concat('%',#{catename},'%')")
    List<YddCate> selectList(String catename);

    void insert(YddCate yddCate);

    void update(YddCate yddCate);

    @Select("select * from advantages_up.ydd_cate where id = #{id}")
    YddCate selectById(Integer id);

    @Delete("delete from advantages_up.ydd_cate where id = #{id}")
    void deleteById(Integer id);
}
