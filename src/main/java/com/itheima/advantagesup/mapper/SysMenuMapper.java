package com.itheima.advantagesup.mapper;

import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.vo.MenuVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SysMenuMapper
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:57
 */
@Mapper
public interface SysMenuMapper {
    /**
     * 选择列表按角色id
     *
     * @param roleIds 角色ids
     * @return {@link List}<{@link SysMenu}>
     */
    List<SysMenu> selectListByRoleIds(List<String> roleIds);

    @Select("select *from advantages_up.sys_menu where pid = #{id}")
    List<SysMenu> selectList(int id);

    String sql = "INSERT INTO 表名 VALUES (值1, 值2, 值3, ...);";

    @Insert("insert into advantages_up.sys_menu (pid, path, component, title, icon, is_show, is_cache, is_link, redirect, create_date, update_date, type, sort, mark)" +
            " VALUES (#{pid}, #{path}, #{component}, #{title}, #{icon}, #{is_show}, #{is_cache}, #{is_link}, #{redirect}, #{createDate}, #{updateDate}, #{type}, #{sort}, #{mark})")
    void insert(SysMenu sysMenu);

    @Delete("delete from advantages_up.sys_menu where id = #{id}")
    void delete(int id);

    @Select("select * from advantages_up.sys_menu where id = #{id}")
    SysMenu selectById(Integer id);

    void update(SysMenu sysMenu);
}
