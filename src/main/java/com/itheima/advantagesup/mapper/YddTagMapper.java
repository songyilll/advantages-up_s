package com.itheima.advantagesup.mapper;

import com.itheima.advantagesup.entity.YddTag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface YddTagMapper {

    @Select("select * from advantages_up.ydd_tag")
    List<YddTag> select();
}
