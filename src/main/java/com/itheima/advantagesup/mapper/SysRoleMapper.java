package com.itheima.advantagesup.mapper;

import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.entity.SysRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SysRoleMapper
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:51
 */
@Mapper
public interface SysRoleMapper {
    /**
     * 选择名单由ids
     *
     * @param roleIds 角色id
     * @return {@link List}<{@link String}>
     */
    List<String> selectNameListByIds(List<String> roleIds);

    List<SysRole> selectList(String name);

    @Select("select * from advantages_up.sys_role")
    List<SysRole> selectAllList();


    void insertSysRole(String id, String name, String description);

    void insertSysRoleMenu(String roleId,Integer menuId);

    @Delete("delete from advantages_up.sys_role_menu where role_id = #{id}")
    void deleteSysRoleMenuByRoleId(String id);

    void updateSysRoleById(SysRole sysRole);

    SysRole selectSysRoleById(String id);

    SysMenu selectSysMenuById(Integer id, Integer pid);

    @Delete("delete from advantages_up.sys_role where id = #{id}")
    void deleteSysRoleByRoleId(String id);
}
