package com.itheima.advantagesup.mapper;

import com.itheima.advantagesup.entity.SysUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SysUserMapper
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 14:59
 */
@Mapper
public interface SysUserMapper {
    /**
     * 选择通过id
     *
     * @param id id
     * @return {@link SysUser}
     */
    SysUser selectById(String id);

    /**
     * 选择用户名
     *
     * @param loginName 登录名
     * @return {@link SysUser}
     */
    SysUser selectByUserName(String loginName);

    /**
     * 更新
     *
     * @param sysUser 系统用户
     */
    void updateById(SysUser sysUser);

    List<SysUser> selectList(String loginName, Integer status, String phone);

    @Select("select name from advantages_up.sys_role where id = #{id}")
    String selectSysRoleNameById(String id);

    void insertSysUser(SysUser sysUser);

    @Delete("delete from advantages_up.sys_user where id = #{id}")
    void deleteById(String id);
}
