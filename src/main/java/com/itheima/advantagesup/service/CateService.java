package com.itheima.advantagesup.service;

import com.itheima.advantagesup.dto.CateDTO;
import com.itheima.advantagesup.vo.CateVO;
import com.itheima.advantagesup.vo.PaginationVO;

import java.util.List;

public interface CateService {
    List<CateVO> all();

    PaginationVO<CateVO> list(String catename, Integer page, Integer limit);

    void add(CateDTO cateDTO);

    void update(CateDTO cateDTO);

    CateVO rowInfo(Integer id);

    void delete(Integer id);
}
