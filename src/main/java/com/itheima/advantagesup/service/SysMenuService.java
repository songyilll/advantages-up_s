package com.itheima.advantagesup.service;

import com.itheima.advantagesup.dto.MenuDTO;
import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.vo.MenuVO;

import java.util.List;

public interface SysMenuService {
    List<MenuVO> allDate();

    void add(MenuDTO menuDTO);

    void del(Integer id);

    MenuVO rowInfo(Integer id);

    void update(MenuDTO menuDTO);
}
