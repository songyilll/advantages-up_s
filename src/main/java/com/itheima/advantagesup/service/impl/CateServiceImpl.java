package com.itheima.advantagesup.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.advantagesup.dto.CateDTO;
import com.itheima.advantagesup.entity.Entity;
import com.itheima.advantagesup.entity.SysRole;
import com.itheima.advantagesup.entity.YddCate;
import com.itheima.advantagesup.mapper.YddCateMapper;
import com.itheima.advantagesup.service.CateService;
import com.itheima.advantagesup.utils.PaginationVOUtil;
import com.itheima.advantagesup.vo.CateVO;
import com.itheima.advantagesup.vo.PaginationVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CateServiceImpl implements CateService {

    private final YddCateMapper yddCateMapper;
    @Override
    public List<CateVO> all() {

        List<YddCate> yddCateList = yddCateMapper.selectAll();

        return yddCateList.stream().map(YddCate::conversion).collect(Collectors.toList());

    }

    @Override
    public PaginationVO<CateVO> list(String catename, Integer page, Integer limit) {

        List<YddCate> yddCateList = yddCateMapper.selectList(catename);
        return PaginationVOUtil.getPaginationVO(page, limit, yddCateList);
    }

    /*private static<T,K> PaginationVO<T> getPaginationVO(Integer page, Integer limit, List<K> yddCateList) {
        PageHelper.startPage(page, limit);
        PageInfo pageInfo = new PageInfo(yddCateList);

        long count = pageInfo.getTotal();
        int totalPages = pageInfo.getPages();
        int pageSize = pageInfo.getPageSize();
        int currentPage = pageInfo.getNextPage();
        List<Entity<T>> yddCateList1 = pageInfo.getList();
        List<T> cateVOList = new ArrayList<>();
        yddCateList1.forEach(yddCate -> cateVOList.add(yddCate.conversion()));

        PaginationVO<T> paginationVO = new PaginationVO<>();
        paginationVO.setCount(count);
        paginationVO.setTotalPages(totalPages);
        paginationVO.setPageSize(pageSize);
        paginationVO.setCurrentPage(currentPage);
        paginationVO.setData(cateVOList);
        return paginationVO;
    }*/

    @Override
    public void add(CateDTO cateDTO) {
        YddCate yddCate = cateDTO.conversion();
        yddCateMapper.insert(yddCate);
    }

    @Override
    public void update(CateDTO cateDTO) {
        YddCate yddCate = cateDTO.conversion();
        yddCateMapper.update(yddCate);
    }

    @Override
    public CateVO rowInfo(Integer id) {
        YddCate yddCate = yddCateMapper.selectById(id);
        return yddCate.conversion();
    }

    @Override
    public void delete(Integer id) {
        yddCateMapper.deleteById(id);
    }
}
