package com.itheima.advantagesup.service.impl;

import com.itheima.advantagesup.dto.MenuDTO;
import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.mapper.SysMenuMapper;
import com.itheima.advantagesup.service.SysMenuService;
import com.itheima.advantagesup.vo.MenuVO;
import lombok.RequiredArgsConstructor;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.databind.type.LogicalType.DateTime;
import static java.time.LocalDateTime.now;

@Service
@RequiredArgsConstructor
public class SysMenuServiceImpl implements SysMenuService {
    private final SysMenuMapper sysMenuMapper;

    @Override
    public List<MenuVO> allDate() {
        List<SysMenu> sysMenuList = sysMenuMapper.selectList(0);
        List<MenuVO> menuVOList = new ArrayList<>();
        sysMenuList.forEach(sysMenu -> menuVOList.add(sysMenu.conversion()));

        menuVOList.forEach(menuVO -> {
            List<SysMenu> sysMenuList1 = sysMenuMapper.selectList(menuVO.getId());;
            List<MenuVO> menuVOList1 = new ArrayList<>();
            sysMenuList1.forEach(sysMenu -> menuVOList1.add(sysMenu.conversion()));

            menuVOList1.forEach(menuVO1 -> {
                List<SysMenu> sysMenuList2 = sysMenuMapper.selectList(menuVO1.getId());
                List<MenuVO> menuVOList2 = new ArrayList<>();
                sysMenuList2.forEach(sysMenu -> menuVOList2.add(sysMenu.conversion()));
                menuVO1.setChildren(menuVOList2);
            });
            menuVO.setChildren(menuVOList1);
        });
        return menuVOList;
    }

    @Override
    public void add(MenuDTO menuDTO) {

        menuDTO.setCreateDate(LocalDateTime.now());
        menuDTO.setUpdateDate(LocalDateTime.now());

        SysMenu sysMenu = menuDTO.conversion();

        sysMenuMapper.insert(sysMenu);
    }

    @Override
    public void del(Integer id) {
        sysMenuMapper.delete(id);
    }

    @Override
    public MenuVO rowInfo(Integer id) {
        return sysMenuMapper.selectById(id).conversion();
    }

    @Override
    public void update(MenuDTO menuDTO) {
        menuDTO.setUpdateDate(LocalDateTime.now());

        SysMenu sysMenu = menuDTO.conversion();

        sysMenuMapper.update(sysMenu);
    }
}
