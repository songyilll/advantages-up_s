package com.itheima.advantagesup.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.advantagesup.dto.RoleDTO;
import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.entity.SysRole;
import com.itheima.advantagesup.entity.SysRoleMenu;
import com.itheima.advantagesup.mapper.SysRoleMapper;
import com.itheima.advantagesup.service.RoleService;
import com.itheima.advantagesup.utils.PaginationVOUtil;
import com.itheima.advantagesup.vo.MenuVO;
import com.itheima.advantagesup.vo.PaginationVO;
import com.itheima.advantagesup.vo.RoleVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final SysRoleMapper sysRoleMapper;

    @Override
    public PaginationVO<RoleVO> list(Integer page, Integer limit, String name) {

        List<SysRole> sysRoleList = sysRoleMapper.selectList(name);

        return PaginationVOUtil.getPaginationVO(page,limit,sysRoleList);
        /*PageHelper.startPage(page, limit);
        PageInfo pageInfo = new PageInfo(sysRoleList);

        long count = pageInfo.getTotal();
        int totalPages = pageInfo.getPages();
        int pageSize = pageInfo.getPageSize();
        int currentPage = pageInfo.getNextPage();
        List<SysRole> sysRoleList1 = pageInfo.getList();
        List<RoleVO> roleVOList = new ArrayList<>();
        sysRoleList1.forEach(sysRole -> roleVOList.add(sysRole.conversion()));

        PaginationVO<RoleVO> paginationVO = new PaginationVO<>();
        paginationVO.setCount(count);
        paginationVO.setTotalPages(totalPages);
        paginationVO.setPageSize(pageSize);
        paginationVO.setCurrentPage(currentPage);
        paginationVO.setData(roleVOList);
        return paginationVO;*/
    }

    @Override
    public List<RoleVO> allData() {

        List<SysRole> sysRoleList = sysRoleMapper.selectAllList();
        List<RoleVO> roleVOList = new ArrayList<>();
        sysRoleList.forEach(sysRole -> roleVOList.add(sysRole.conversion()));
        return roleVOList;
    }

    @Override
    public void add(RoleDTO roleDTO) {

        String uuid = UUID.randomUUID().toString();
        sysRoleMapper.insertSysRole(uuid, roleDTO.getName(), roleDTO.getDescription());

        for (Integer menuId : roleDTO.getMenuids()) {
            sysRoleMapper.insertSysRoleMenu(uuid, menuId);
        }
    }

    @Override
    public void update(RoleDTO roleDTO) {

        sysRoleMapper.deleteSysRoleMenuByRoleId(roleDTO.getId());

        SysRole sysRole = roleDTO.conversion();

        sysRoleMapper.updateSysRoleById(sysRole);
        Integer[] menuIds = roleDTO.getMenuids();

        for (Integer menuId : menuIds) {
            sysRoleMapper.insertSysRoleMenu(roleDTO.getId(), menuId);
        }
    }

    @Override
    public RoleVO rowInfo(String id) {

        SysRole sysRole = sysRoleMapper.selectSysRoleById(id);
        List<MenuVO> menuVOList1 = new ArrayList<>();
        if (!sysRole.getRoleMenus().isEmpty()) {
            String[] menuIds = sysRole.getRoleMenus().split(",");

            for (String s1 : menuIds) {
                SysMenu sysMenu1 = sysRoleMapper.selectSysMenuById(Integer.parseInt(s1), 0);
                if (sysMenu1 != null) {
                    MenuVO menuVO1 = sysMenu1.conversion();
                    List<MenuVO> menuVOList2 = new ArrayList<>();

                    for (String s2 : menuIds) {
                        SysMenu sysMenu2 = sysRoleMapper.selectSysMenuById(Integer.parseInt(s2), menuVO1.getId());
                        if (sysMenu2 != null) {
                            MenuVO menuVO2 = sysMenu2.conversion();
                            List<MenuVO> menuVOList3 = new ArrayList<>();

                            for (String s3 : menuIds) {
                                SysMenu sysMenu3 = sysRoleMapper.selectSysMenuById(Integer.parseInt(s3), menuVO2.getId());
                                if (sysMenu3 != null) {
                                    MenuVO menuVO3 = sysMenu3.conversion();
                                    menuVOList3.add(menuVO3);
                                }
                            }
                            menuVO2.setChildren(menuVOList3);
                            menuVOList2.add(menuVO2);
                        }
                    }
                    menuVO1.setChildren(menuVOList2);
                    menuVOList1.add(menuVO1);
                }
            }
        }
        RoleVO roleVO = sysRole.conversion();
        roleVO.setAuthList(menuVOList1);
        return roleVO;
    }

    @Override
    public void delete(String id) {
        sysRoleMapper.deleteSysRoleMenuByRoleId(id);
        sysRoleMapper.deleteSysRoleByRoleId(id);
    }
}
