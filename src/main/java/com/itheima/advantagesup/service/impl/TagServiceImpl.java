package com.itheima.advantagesup.service.impl;

import com.itheima.advantagesup.entity.YddTag;
import com.itheima.advantagesup.mapper.YddTagMapper;
import com.itheima.advantagesup.service.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final YddTagMapper yddTagMapper;
    @Override
    public List<YddTag> add() {
        return yddTagMapper.select();
    }
}
