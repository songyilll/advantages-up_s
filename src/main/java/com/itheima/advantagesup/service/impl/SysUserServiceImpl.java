package com.itheima.advantagesup.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.advantagesup.dto.UserDTO;
import com.itheima.advantagesup.entity.SysRole;
import com.itheima.advantagesup.entity.SysUser;
import com.itheima.advantagesup.mapper.SysUserMapper;
import com.itheima.advantagesup.service.SysUserService;
import com.itheima.advantagesup.utils.PaginationVOUtil;
import com.itheima.advantagesup.vo.PaginationVO;
import com.itheima.advantagesup.vo.RoleVO;
import com.itheima.advantagesup.vo.UserInfoVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * SysUserServiceImpl
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 14:58
 */
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl implements SysUserService {

    private final SysUserMapper sysUserMapper;
    @Override
    public PaginationVO<UserInfoVO> list(String loginName, Integer status, String phone, Integer page, Integer limit) {

        List<SysUser> sysUserList = sysUserMapper.selectList(loginName,status,phone);
        PaginationVO<UserInfoVO> paginationVO = PaginationVOUtil.getPaginationVO(page, limit, sysUserList);
        paginationVO.getData().forEach(p ->{
            List<String> stringList = new ArrayList<>();
            p.getRole_id().forEach(g ->{
                String roleName = sysUserMapper.selectSysRoleNameById(g);
                stringList.add(roleName);
            });
            p.setRolename(stringList);
        });
        return paginationVO;
    }

    @Override
    public void add(UserDTO userDTO) {
        SysUser sysUser = userDTO.conversion();
        String uuid = UUID.randomUUID().toString();
        sysUser.setId(uuid);
        sysUserMapper.insertSysUser(sysUser);
    }

    @Override
    public void update(UserDTO userDTO) {
        SysUser sysUser = userDTO.conversion();
        sysUserMapper.updateById(sysUser);
    }

    @Override
    public UserInfoVO rowInfo(String id) {
        SysUser sysUser = sysUserMapper.selectById(id);
        List<String> stringList1 = new ArrayList<>(Arrays.asList(sysUser.getRoleId().replace("\"", "").replace("[", "").replace("]", "").split(",")));
        UserInfoVO userInfoVO = sysUser.conversion();
        userInfoVO.setRole_id(stringList1);
        return userInfoVO;
    }

    @Override
    public void delete(String id) {
        sysUserMapper.deleteById(id);
    }

}
