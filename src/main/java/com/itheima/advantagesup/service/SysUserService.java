package com.itheima.advantagesup.service;

import com.itheima.advantagesup.dto.UserDTO;
import com.itheima.advantagesup.vo.PaginationVO;
import com.itheima.advantagesup.vo.UserInfoVO;

/**
 * SysUserService
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 14:58
 */
public interface SysUserService {
    PaginationVO<UserInfoVO> list(String loginName, Integer status, String phone, Integer page, Integer limit);

    void add(UserDTO userDTO);

    void update(UserDTO userDTO);

    UserInfoVO rowInfo(String id);

    void delete(String id);
}
