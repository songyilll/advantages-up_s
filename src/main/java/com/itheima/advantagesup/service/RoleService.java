package com.itheima.advantagesup.service;

import com.itheima.advantagesup.dto.RoleDTO;
import com.itheima.advantagesup.vo.PaginationVO;
import com.itheima.advantagesup.vo.RoleVO;

import java.util.List;

public interface RoleService {
    PaginationVO<RoleVO> list(Integer page, Integer limit, String name);

    List<RoleVO> allData();

    void add(RoleDTO roleDTO);

    void update(RoleDTO roleDTO);

    RoleVO rowInfo(String id);

    void delete(String id);
}
