package com.itheima.advantagesup.vo;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户信息签证官
 *
 * @author liudo
 * @date 2023/07/26
 */
@Data
public class UserInfoVO {
    /**
     * id
     */
    private String id;
    /**
     * 登录名
     */
    private String login_name;
    /**
     * 密码
     */
    private String password;
    /**
     * 名字
     */
    private String name;
    /**
     * 电子邮件
     */
    private String email;
    /**
     * 电话
     */
    private String phone;
    /**
     * 登录知识产权
     */
    private String login_ip;
    /**
     * 登录日期
     */
    private LocalDateTime login_date;
    /**
     * 创建日期
     */
    private LocalDateTime create_date;
    /**
     * 更新日期
     */
    private LocalDateTime update_date;
    /**
     * 删除标志
     */
    private String del_flag;
    /**
     * 角色id
     */
    private List<String> role_id;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 角色名称
     */
    private List<String> rolename;
}
