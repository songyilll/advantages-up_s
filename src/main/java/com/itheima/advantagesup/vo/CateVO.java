package com.itheima.advantagesup.vo;

import lombok.Data;

@Data
public class CateVO {
    private Integer id;
    private String catename;
    private String icon;
    private Integer sort_num;
    private String default_data;
}
