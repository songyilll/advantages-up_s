package com.itheima.advantagesup.vo;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class RoleVO {
    private String id;
    private String name;
    private String description;
    private LocalDateTime create_date;
    private LocalDateTime update_date;
    private String remarks;
    private String del_flag;
    private Integer superadmin;
    private String default_data;
    private String role_id;
    private String role_menus;
    private List<MenuVO> authList;
}
