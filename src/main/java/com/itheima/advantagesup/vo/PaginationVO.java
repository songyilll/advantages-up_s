package com.itheima.advantagesup.vo;

import com.itheima.advantagesup.entity.SysRole;
import lombok.Data;

import java.util.List;

@Data
public class PaginationVO<T> {
    private Long count;
    private Integer totalPages;
    private Integer pageSize;
    private Integer currentPage;
    private List<T> data;
}
