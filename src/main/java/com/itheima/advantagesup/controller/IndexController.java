package com.itheima.advantagesup.controller;

import com.itheima.advantagesup.common.R;
import com.itheima.advantagesup.dto.UserLoginDTO;
import com.itheima.advantagesup.service.IndexService;
import com.itheima.advantagesup.vo.ProfileVO;
import com.itheima.advantagesup.vo.UserLoginVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * IndexController
 *
 * @author liliudong
 * @version 1.0
 * @description 首页控制器
 * @date 2023/7/26 16:42
 */
@RestController
@RequestMapping("/index")
@RequiredArgsConstructor
@Slf4j
public class IndexController {

    private final IndexService indexService;

    /**
     * 登录
     *
     * @param userLoginDTO 用户登录dto
     * @return {@link R}<{@link ?}>
     */
    @PostMapping("/login")
    public R<?> login(@RequestBody UserLoginDTO userLoginDTO) {
        UserLoginVO userLoginVO = indexService.login(userLoginDTO);
        return R.success(userLoginVO);
    }

    /**
     * 获取首页登录数据
     *
     * @return {@link R}<{@link ?}>
     */
    @PostMapping("/profile")
    public R<?> profile() {
        ProfileVO profileVO = indexService.profile();

        return R.success(profileVO);
    }
}
