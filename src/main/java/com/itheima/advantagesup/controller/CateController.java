package com.itheima.advantagesup.controller;


import com.itheima.advantagesup.common.R;
import com.itheima.advantagesup.dto.CateDTO;
import com.itheima.advantagesup.service.CateService;
import com.itheima.advantagesup.vo.CateVO;
import com.itheima.advantagesup.vo.PaginationVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/ydd_cate")
@RequiredArgsConstructor
public class CateController {

    private final CateService cateService;

    @GetMapping("/all")
    public R<List<CateVO>> all(){
        List<CateVO> cateVOList = cateService.all();
        return R.success(cateVOList);
    }

    @GetMapping("/list")
    public R<PaginationVO<CateVO>> list(String catename, Integer page, Integer limit){
        log.info("接收到数据,{},{},{},",catename,page,limit);
        PaginationVO<CateVO> paginationVO = cateService.list(catename,page,limit);
        return R.success(paginationVO);
    }

    @PostMapping("/add")
    public R<?> add(@RequestBody CateDTO cateDTO){
        log.info("接收到数据,{},",cateDTO);
        cateService.add(cateDTO);
        return R.success();
    }

    @PostMapping("/update")
    public R<?> update(@RequestBody CateDTO cateDTO){
        log.info("接收到数据,{},",cateDTO);
        cateService.update(cateDTO);
        return R.success();
    }

    @GetMapping("/rowInfo")
    public R<CateVO> rowInfo(Integer id){
        log.info("接收到数据,{},",id);
        CateVO cateVO = cateService.rowInfo(id);
        return R.success(cateVO);
    }

    @GetMapping("del")
    public R<?> delete(Integer id){
        log.info("接收到数据,{},",id);
        cateService.delete(id);
        return R.success();
    }
}
