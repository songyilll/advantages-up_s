package com.itheima.advantagesup.controller;

import com.itheima.advantagesup.common.R;
import com.itheima.advantagesup.dto.UserDTO;
import com.itheima.advantagesup.service.SysUserService;
import com.itheima.advantagesup.vo.PaginationVO;
import com.itheima.advantagesup.vo.UserInfoVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/sys_user")
@RequiredArgsConstructor
public class UserController {

    private final SysUserService sysUserService;

    @GetMapping("/list")
    public R<PaginationVO<UserInfoVO>> list(String login_name, Integer status, String phone, Integer page, Integer limit) {
        log.info("接收到数据,{},{},{},{},{},", login_name, status, phone, page, limit);
        PaginationVO<UserInfoVO> paginationVO = sysUserService.list(login_name, status, phone, page, limit);
        return R.success(paginationVO);
    }

    @PostMapping("/add")
    public R<?> add (@RequestBody UserDTO userDTO){
        log.info("接收到数据,{},",userDTO);
        sysUserService.add(userDTO);
        return R.success();
    }

    @PostMapping("/update")
    public R<?> update(@RequestBody UserDTO userDTO){
        log.info("接收到数据,{},",userDTO);
        sysUserService.update(userDTO);
        return R.success();
    }

    @GetMapping("/rowInfo")
    public R<UserInfoVO> rowInfo(String id){
        log.info("接收到数据,{},",id);
        UserInfoVO userInfoVO = sysUserService.rowInfo(id);
        return R.success(userInfoVO);
    }

    @GetMapping("/del")
    public R<?> delete(String id){
        log.info("接收到数据,{},",id);
        sysUserService.delete(id);
        return R.success();
    }
}
