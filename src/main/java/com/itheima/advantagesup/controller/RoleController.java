package com.itheima.advantagesup.controller;

import com.itheima.advantagesup.common.R;
import com.itheima.advantagesup.dto.RoleDTO;
import com.itheima.advantagesup.service.RoleService;
import com.itheima.advantagesup.vo.PaginationVO;
import com.itheima.advantagesup.vo.RoleVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/sys_role")
@RequiredArgsConstructor
public class RoleController {
    private final RoleService roleService;
    @GetMapping("/list")
    public R<PaginationVO<RoleVO>> list(Integer page, Integer limit, String name) {
        log.info("接收到数据:{},{},{},", page, limit, name);
        PaginationVO<RoleVO> paginationVO = roleService.list(page, limit, name);
        return R.success(paginationVO);
    }
    @GetMapping("/allData")
    public R<List<RoleVO>> allData() {
        List<RoleVO> roleVOList = roleService.allData();
        return R.success(roleVOList);
    }
    @PostMapping("/add")
    public R<?> add(@RequestBody RoleDTO roleDTO) {
        log.info("接收到数据,{}", roleDTO);
        roleService.add(roleDTO);
        return R.success();
    }
    @PostMapping("/update")
    public R<?> update(@RequestBody RoleDTO roleDTO) {
        log.info("接收到数据,{}", roleDTO);
        roleService.update(roleDTO);
        return R.success();
    }
    @GetMapping("/rowInfo")
    public R<RoleVO> rowInfo(String id){
        log.info("接收到数据,{}", id);
        RoleVO roleVO = roleService.rowInfo(id);
        return R.success(roleVO);
    }
    @GetMapping("/del")
    public R<?> del(String id){
        log.info("接收到数据,{}", id);
        roleService.delete(id);
        return R.success();
    }
}
