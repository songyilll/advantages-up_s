package com.itheima.advantagesup.controller;


import com.itheima.advantagesup.common.R;
import com.itheima.advantagesup.dto.MenuDTO;
import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.service.SysMenuService;
import com.itheima.advantagesup.vo.MenuVO;
import com.itheima.advantagesup.vo.ProfileVO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sys_menu")
@RequiredArgsConstructor
public class SysMenuController {
    private final SysMenuService sysMenuService;

    /**
     * 所有菜单
     *
     * @return {@link R}<{@link ProfileVO}>
     */
    @GetMapping("/allData")
    public R<List<MenuVO>> allDate(){
        List<MenuVO> menuVOList = sysMenuService.allDate();
        return R.success(menuVOList);
    }


    @PostMapping("add")
    public R<?> add (@RequestBody MenuDTO menuDTO){
        sysMenuService.add(menuDTO);
        return R.success();
    }

    @GetMapping("/del")
    public R<?> del(Integer id){
        sysMenuService.del(id);
        return R.success();
    }

    @GetMapping("/rowInfo")
    public R<MenuVO> rowInfo(Integer id){
        MenuVO menuVO = sysMenuService.rowInfo(id);
        return R.success(menuVO);
    }

    @PostMapping("/update")
    public R<?> update(@RequestBody MenuDTO menuDTO){
        sysMenuService.update(menuDTO);
        return R.success();
    }
}
