package com.itheima.advantagesup.controller;


import com.itheima.advantagesup.common.R;
import com.itheima.advantagesup.entity.YddTag;
import com.itheima.advantagesup.service.TagService;
import com.itheima.advantagesup.vo.PaginationVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/ydd_tag")
@RequiredArgsConstructor
public class TagController {

    private final TagService tagService;

    @GetMapping("/all")
    private R<List<YddTag>> all() {
        List<YddTag> yddTagList = tagService.add();
        return R.success(yddTagList);
    }
}
