package com.itheima.advantagesup.utils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.advantagesup.entity.Entity;
import com.itheima.advantagesup.mapper.SysUserMapper;
import com.itheima.advantagesup.vo.PaginationVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaginationVOUtil {

    public static<T,K> PaginationVO<T> getPaginationVO(Integer page, Integer limit, List<K> list) {
        PageHelper.startPage(page, limit);
        PageInfo pageInfo = new PageInfo(list);

        long count = pageInfo.getTotal();
        int totalPages = pageInfo.getPages();
        int pageSize = pageInfo.getPageSize();
        int currentPage = pageInfo.getNextPage();
        List<Entity<T>> entityList = pageInfo.getList();

        List<T> tList = new ArrayList<>();
        entityList.forEach(entity -> tList.add(entity.conversion()));

        PaginationVO<T> paginationVO = new PaginationVO<>();
        paginationVO.setCount(count);
        paginationVO.setTotalPages(totalPages);
        paginationVO.setPageSize(pageSize);
        paginationVO.setCurrentPage(currentPage);
        paginationVO.setData(tList);
        return paginationVO;
    }
}
